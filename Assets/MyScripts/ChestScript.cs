using UnityEngine;

public class ChestScript : MonoBehaviour
{

    public int goldAmt = 50;

    private Animator anim;

    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && InventoryItems.key && goldAmt > 0)
        {
            anim.SetTrigger("open");
            InventoryItems.gold += goldAmt;
            goldAmt = 0;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && InventoryItems.key)
        {
            anim.SetTrigger("close");
        }
    }

    public void DestroyChest()
    {
        Destroy(gameObject);
    }
}
