using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HintMessage : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public GameObject hintBox;
    public Text message;
    private Vector3 screenPoint;
    public int objectType = 0;

    public void OnPointerEnter(PointerEventData eventData)
    {
        hintBox.SetActive(true);
        screenPoint.x = Input.mousePosition.x + 400;
        screenPoint.y = Input.mousePosition.y;
        screenPoint.z = 1f;
        MessageDisplay();
        hintBox.transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hintBox.SetActive(false);

    }

    void Start()
    {
        hintBox.SetActive(false);
    }

    private void MessageDisplay()
    {
        switch (objectType)
        {
            case 1:
                message.text = InventoryItems.redMushrooms.ToString() + " red mushrooms to be used in potions";
                break;
            case 2:
                message.text = InventoryItems.purpleMushrooms.ToString() + " purple mushrooms to be used in potions";
                break;
            case 3:
                message.text = InventoryItems.brownMushrooms.ToString() + " brown mushrooms to be used in potions";
                break;
            case 4:
                message.text = InventoryItems.blueFlowers.ToString() + " blue flowers to be used in potions";
                break;
            case 5:
                message.text = InventoryItems.redFlowers.ToString() + " red flowers to be used in potions";
                break;
            case 6:
                message.text = InventoryItems.roots.ToString() + " roots to be used in potions";
                break;
            case 7:
                message.text = InventoryItems.leafDew.ToString() + " leaf dew to be used in potions";
                break;
            case 8:
                message.text = "Key to open chests";
                break;
            case 9:
                message.text = InventoryItems.dragonEgg.ToString() + " dragon eggs to be used in potions";
                break;
            case 10:
                message.text = InventoryItems.bluePotion.ToString() + " blue potion to be used in potions";
                break;
            case 11:
                message.text = InventoryItems.purplePotion.ToString() + " purple potion to be used in potions";
                break;
            case 12:
                message.text = InventoryItems.greenPotion.ToString() + " green potion to be used in potions";
                break;
            case 13:
                message.text = InventoryItems.redPotion.ToString() + " red potion to be used in potions";
                break;
            case 14:
                message.text = InventoryItems.bread.ToString() + " bread used to replenish health";
                break;
            case 15:
                message.text = InventoryItems.cheese.ToString() + " cheese used to replenish health";
                break;
            case 16:
                message.text = InventoryItems.meat.ToString() + " meat used to replenish health";
                break;
            case 0:
            default:
                message.text = "Empty";
                break;
        }
    }
}
