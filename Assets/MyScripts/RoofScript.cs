using UnityEngine;

public class RoofScript : MonoBehaviour
{

    public GameObject roof;
    public GameObject props;
    // Start is called before the first frame update
    void Start()
    {
        roof.SetActive(true);
        props.SetActive(false);
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            roof.SetActive(false);
            props.SetActive(true);
        }
    }
}
